def measure(n=1, &prc)
  beginning_time = Time.now
  n.times { prc.call }
  (Time.now - beginning_time) / n
end
