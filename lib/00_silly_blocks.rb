def reverser(&prc)
  sentence = prc.call
  sentence.split.map(&:reverse).join(" ")
end

def adder(val=1, &prc)
  prc.call + val
end

def repeater(number=1, &prc)
  number.times { prc.call }

end
